#!/usr/bin/env python3

from time import sleep
from azure.storage.queue import QueueClient
import logging
import os

# read config
logging.debug("Starting order_service")

# docker
storage_account_name = os.environ.get('STORAGE_NAME')
storage_account_key = os.environ.get('KEY')
queue_name = os.environ.get('QUEUE_NAME')

queue_service = QueueClient(account_url=f"https://{storage_account_name}.queue.core.windows.net/", credential=storage_account_key, queue_name=queue_name)

logging.debug("Started order_service")
while True:
    messages = queue_service.receive_messages(queue_name=queue_name, number_of_messages=1)
    for message in messages:
        queue_service.delete_message(queue_name=queue_name, message_id=message.id, pop_receipt=message.pop_receipt)
        logging.debug("Deleted a message")
    
    if not messages:
        logging.debug("Nothing to do")
    sleep(60)
