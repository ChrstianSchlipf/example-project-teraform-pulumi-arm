using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Npgsql;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace DatenbankSteuerung
{
    public static class Function1
    {
        private const string Host = "permissiondatabase.postgres.database.azure.com";
        private const string User = "PerAdmin";
        private const string DBname = "postgres";
        private const string Password = "9bAADtQbc65udP87iPvZtqAvqknFgCW5xfdumhc7Aaga9BiF08th5BnFyWa3Dh7X4aD";
        private const string Port = "5432";

        private const string ConnString = $"Server={Host}; User Id={User}; Database={DBname}; Port={Port}; Password={Password};SSLMode=Prefer";


        [FunctionName("DBRestart")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request");


            await using var conn = new NpgsqlConnection(ConnString);
            Console.Out.WriteLine("Opening connection");
            conn.Open();


            await using (var command = new NpgsqlCommand("DROP TABLE IF EXISTS users", conn))
            {
                command.ExecuteNonQuery();
                Console.Out.WriteLine("Finished dropping table (if existed) users");

            }

            await using (var command = new NpgsqlCommand("CREATE TABLE users(name VARCHAR(255), password VARCHAR(255))", conn))
            {
                command.ExecuteNonQuery();
                Console.Out.WriteLine("Finished creating table users");
            }


            await using (var command = new NpgsqlCommand("DROP TABLE IF EXISTS products", conn))
            {
                command.ExecuteNonQuery();
                Console.Out.WriteLine("Finished dropping table (if existed) products");

            }

            await using (var command = new NpgsqlCommand("CREATE TABLE products(id INTEGER, name VARCHAR(255))", conn))
            {
                command.ExecuteNonQuery();
                Console.Out.WriteLine("Finished creating table products");
            }


            await using (var command = new NpgsqlCommand("DROP TABLE IF EXISTS orders", conn))
            {
                command.ExecuteNonQuery();
                Console.Out.WriteLine("Finished dropping table (if existed) orders");

            }

            await using (var command = new NpgsqlCommand("CREATE TABLE orders(orderid INTEGER, productid INTEGER, username VARCHAR(255))", conn))
            {
                command.ExecuteNonQuery();
                Console.Out.WriteLine("Finished creating table orders");
            }
            return new OkResult();
        }


        [FunctionName("Login")]
        public static async Task<IActionResult> Run1(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request");

            var body = req.Body;
            var streamReader = new StreamReader(body);
            var input = streamReader.ReadToEnd();
            dynamic json = JObject.Parse(input);
            var usernameJson = json.Username;
            var passwordJson = json.Password;
            string username = usernameJson.ToString();
            string password = passwordJson.ToString();

            var passwordDatabase = "";


            await using var conn = new NpgsqlConnection(ConnString);
            Console.Out.WriteLine("Opening connection");
            conn.Open();

            await using (var command = new NpgsqlCommand("SELECT password FROM users WHERE name = @n", conn))
            {
                command.Parameters.AddWithValue("n", username);
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    passwordDatabase = reader.GetString(0);
                }
                reader.Close();
            }


            if (password == passwordDatabase)
            {
                return new OkResult();
            }
            else
            {
                return new UnauthorizedResult();
            }
        }


        [FunctionName("CreateAccount")]
        public static async Task<IActionResult> Run2(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request");

            var body = req.Body;
            var streamReader = new StreamReader(body);
            var input = streamReader.ReadToEnd();
            dynamic json = JObject.Parse(input);
            var usernameJson = json.Username;
            var passwordJson = json.Password;
            string username = usernameJson.ToString();
            string password = passwordJson.ToString();

            var usernameDatabase = "";


            await using var conn = new NpgsqlConnection(ConnString);
            Console.Out.WriteLine("Opening connection");
            conn.Open();

            await using (var command = new NpgsqlCommand("SELECT name FROM users WHERE name = @n", conn))
            {
                command.Parameters.AddWithValue("n", username);
                  
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    usernameDatabase = reader.GetString(0);
                }

                reader.Close();
            }

            if (usernameDatabase == username)
            {
                return new ConflictObjectResult("Account exists already");
            }


            await using (var command = new NpgsqlCommand("INSERT INTO users (name, password) VALUES (@n, @p)", conn))
            {
                command.Parameters.AddWithValue("n", username);
                command.Parameters.AddWithValue("p", password);
                var nRows = command.ExecuteNonQuery();

                Console.WriteLine($"Number of rows inserted={nRows}");
            }

            return new OkResult();
        }


        [FunctionName("CreateProduct")]
        public static async Task<IActionResult> Run3(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request");

            var body = req.Body;
            var streamReader = new StreamReader(body);
            var input = streamReader.ReadToEnd();
            dynamic json = JObject.Parse(input);
            var idJson = json.Productid;
            var nameJson = json.Productname;
            int id = idJson.ToString();
            string productname = nameJson.ToString();

            var idDatabase = -1;

            await using var conn = new NpgsqlConnection(ConnString);
            Console.Out.WriteLine("Opening connection");
            conn.Open();

            await using (var command = new NpgsqlCommand("SELECT id FROM products WHERE id = @id", conn))
            {
                command.Parameters.AddWithValue("id", id);
                   
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    idDatabase = reader.GetInt32(0);
                }

                reader.Close();
            }

            if (id == idDatabase)
            {
                return new ConflictObjectResult("Productid exists already");
            }


            await using (var command = new NpgsqlCommand("INSERT INTO products (id, name) VALUES (@id, @n)", conn))
            {
                command.Parameters.AddWithValue("id", id);
                command.Parameters.AddWithValue("n", productname);
                var nRows = command.ExecuteNonQuery();

                Console.WriteLine($"Number of rows inserted={nRows}");
            }

            return new OkResult();
        }


        [FunctionName("CreateOrder")]
        public static async Task<IActionResult> Run4(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request");

            var body = req.Body;
            var streamReader = new StreamReader(body);
            var input = streamReader.ReadToEnd();
            dynamic json = JObject.Parse(input);

            var idJson = json.Orderid;
            var productJson = json.Productname;
            var userJson = json.Username;

            int id = idJson.ToString();
            int productid = productJson.ToString();
            string username = userJson.ToString();

            var idDatabase = -1;
            var productIdDb = -1;
            var usernameDb = "";
            if (usernameDb == null) throw new ArgumentNullException(nameof(usernameDb));

            await using (var conn = new NpgsqlConnection(ConnString))

            {
                Console.Out.WriteLine("Opening connection");
                conn.Open();

                await using (var command = new NpgsqlCommand("SELECT name FROM users WHERE name = @n", conn))
                {
                    command.Parameters.AddWithValue("n", username);
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        usernameDb = reader.GetString(0);
                    }

                    reader.Close();
                }

                if (username != usernameDb)
                {
                    return new ConflictObjectResult("User doesnt exists");
                }

                await using (var command = new NpgsqlCommand("SELECT id FROM products WHERE id = @id", conn))
                {
                    command.Parameters.AddWithValue("id", productid);
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        productIdDb = reader.GetInt32(0);
                    }

                    reader.Close();
                }

                if (productid != productIdDb)
                {
                    return new ConflictObjectResult("Product doesnt exists");
                }


                await using (var command = new NpgsqlCommand("SELECT orderid FROM orders WHERE orderid = @id", conn))
                {
                    command.Parameters.AddWithValue("id", id);
                    
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        idDatabase = reader.GetInt32(0);
                    }

                    reader.Close();
                }

                if (id == idDatabase)
                {
                    return new ConflictObjectResult("Orderid exists already");
                }


                await using (var command = new NpgsqlCommand("INSERT INTO orders (orderid, productid, username) VALUES (@id, @p, @n)", conn))
                {
                    command.Parameters.AddWithValue("id", id);
                    command.Parameters.AddWithValue("p", productid);
                    command.Parameters.AddWithValue("n", username);

                    var nRows = command.ExecuteNonQuery();

                    Console.WriteLine($"Number of rows inserted={nRows}");
                }

                return new OkResult();
            }
        }



        [FunctionName("GetProduct")]
        public static async Task<IActionResult> Run5(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request");

            var body = req.Body;
            var streamReader = new StreamReader(body);
            var input = streamReader.ReadToEnd();
            dynamic json = JObject.Parse(input);

            var idJson = json.Productid;
            int id = idJson.ToString();
           
            
            var returnList = new List<(int, string)> ();

            await using var conn = new NpgsqlConnection(ConnString);
            Console.Out.WriteLine("Opening connection");
            conn.Open();

            await using (var command = new NpgsqlCommand("SELECT * FROM products WHERE id = @id", conn))
            {
                command.Parameters.AddWithValue("id", id);
                  
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    returnList.Add((reader.GetInt32(0), reader.GetString(1)));
                }

                reader.Close();
            }

            if (returnList.Count == 0)
            {
                return new NotFoundObjectResult("Product not found");
            }
            return new OkObjectResult(returnList);
        }


        [FunctionName("GetOrder")]
        public static async Task<IActionResult> Run6(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request");

            var body = req.Body;
            var streamReader = new StreamReader(body);
            var input = streamReader.ReadToEnd();
            dynamic json = JObject.Parse(input);

            var idJson = json.OrderId;
            int id = idJson.ToString();


            var returnList = new List<(int, int, string)>();

            await using var conn = new NpgsqlConnection(ConnString);
            Console.Out.WriteLine("Opening connection");
            conn.Open();

            await using (var command = new NpgsqlCommand("SELECT * FROM orders WHERE orderid = @id", conn))
            {
                command.Parameters.AddWithValue("id", id);

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    returnList.Add((reader.GetInt32(0), reader.GetInt32(1), reader.GetString(2)));
                }

                reader.Close();
            }

            if (returnList.Count == 0)
            {
                return new NotFoundObjectResult("Order not found");
            }
            else
            {
                return new OkObjectResult(returnList);
            }
        }



        [FunctionName("GetProducts")]
        public static async Task<IActionResult> Run7(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request");


            var returnList = new List<(int, string)>();

            await using var conn = new NpgsqlConnection(ConnString);
            Console.Out.WriteLine("Opening connection");
            conn.Open();

            await using (var command = new NpgsqlCommand("SELECT * FROM products", conn))
            {
                   
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    returnList.Add((reader.GetInt32(0), reader.GetString(1)));
                }

                reader.Close();
            }

            if (returnList.Count == 0)
            {
                return new NotFoundObjectResult("No Products found");
            }
            return new OkObjectResult(returnList);
        }



        [FunctionName("GetOrders")]
        public static async Task<IActionResult> Run8(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request");


            var returnList = new List<(int, int, string)>();

            await using var conn = new NpgsqlConnection(ConnString);
            Console.Out.WriteLine("Opening connection");
            conn.Open();

            await using (var command = new NpgsqlCommand("SELECT * FROM orders", conn))
            {

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    returnList.Add((reader.GetInt32(0), reader.GetInt32(1), reader.GetString(2)));
                }

                reader.Close();
            }

            if (returnList.Count == 0)
            {
                return new NotFoundObjectResult("No Orders found");
            }
            else
            {
                return new OkObjectResult(returnList);
            }
        }
    }
}
